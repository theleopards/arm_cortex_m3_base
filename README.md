# SourceTemplate: **arm_cortex_m3_base**

## **Development Kit**
##### AK STM32L151CBT6 TOP
![image](application/doc/hardware/images/AK STM32L151CBT6 TOP.jpg)

##### AK STM32L151CBT6 BOTTOM
![image](application/doc/hardware/images/AK STM32L151CBT6 BOTTOM.jpg)

## **Demo Videos**
| Video | Link |
| ------ | ------ |
| Soap Bubbles | [Soap Bubbles Video](application/doc/hardware/videos/AK Soap Bubbles.mp4) |